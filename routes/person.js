const express = require('express');
const multer = require('multer');
var pathRoot = require('../app')
var path = require('path')



const router = express.Router();

const personController = require('../controllers/person');
const VerifyToken = require('../config/verifyToken');

var storage = multer.diskStorage({
    destination : function(req,file,callback){
        callback(null, pathRoot.path);
    },
    filename: function(req,file,callback){
        callback(null, 'personas-no-deseadas' + '-' + new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '')  + path.extname(file.originalname));
    }
});

var upload = multer({ storage : storage});

router.get('/',VerifyToken, personController.getPerson);
router.get('/:id',VerifyToken, personController.getPerson);
router.post('/',VerifyToken, personController.createPerson);
router.put('/:id',VerifyToken, personController.updatePerson);
router.post('/search',VerifyToken, personController.searchPerson);
router.post('/excel', upload.single('excel'), personController.readExcel);
router.delete('/:idPerson',VerifyToken, personController.deletePersona);


module.exports = router;