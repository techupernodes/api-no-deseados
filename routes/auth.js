const express = require('express');

const router = express.Router();

const homeController = require('../controllers/auth');

router.get('/', homeController.index);

router.post('/login', homeController.postLogin);

router.get('/logout', homeController.logout);

module.exports = router;