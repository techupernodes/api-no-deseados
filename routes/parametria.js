const express = require('express');

const router = express.Router();

const brandController = require('../controllers/parametria');
const VerifyToken = require('../config/verifyToken');


router.get('/',VerifyToken,brandController.getBrand);
router.get('/:id',VerifyToken,brandController.getBrand);
router.post('/',VerifyToken, brandController.createBrand);
router.delete('/:id',VerifyToken, brandController.deleteBrand);
router.put('/:id',VerifyToken,brandController.updateBrand);

module.exports = router;
