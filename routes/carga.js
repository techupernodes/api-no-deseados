const express = require('express');
const multer = require('multer');
var path = require('path')


const router = express.Router();

const cargaController = require('../controllers/carga');
const VerifyToken = require('../config/verifyToken');

router.get('/file/:file', cargaController.getFile);


module.exports = router;