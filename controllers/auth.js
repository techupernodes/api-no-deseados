
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const dotenv = require('dotenv');
var requestJSON = require('request-json');

dotenv.load({ path: '.env' });

const api_key = process.env.API_KEY;
const api_context = process.env.API_CONTEXT;
const token_key = process.env.API_KEY_TECHU
var BCRYPT_SALT_ROUNDS = 12;

exports.index = async (req, res) => {
  try {
    res.send('API Practitioner Personas no deseadas');
  } catch (error) {
    res.send(error);
  }
}

function login(req, res) {
  try {
    var isLogin = new Object();
    isLogin.email = req.body.email;
    var passwordhash = req.body.password;

    let query = 'q=' + JSON.stringify(isLogin) + '&';
    let httpClient = requestJSON.createClient(api_context);
    httpClient.get('users?' + query + api_key,
      function (err, respuestaMLab, body) {
        if (body[0] == undefined) return res.status(401).send({ auth: "Email incorrecto" })

        bcrypt.compare(passwordhash, body[0].password, function (err, result) {
          console.log('hash is ' + result);
          if (result) {
            var token = jwt.sign({ id: body[0]._id }, token_key, {
              expiresIn: 7200 // tiempo en segundos
            });
            res.status(200).json({
              token: token,
              nombreusuario: body[0].first_name + ' ' + body[0].last_name
            });
          } else {
            res.status(401).send({
              auth: "Contraseña incorrecta"
            });
          }

        });
      });
  } catch (error) {
    res.status(500).json(error);
  }

};

exports.postLogin = login;

exports.logout = async (req, res) => {
  res.status(200).send({ auth: false, token: null });
}