const dotenv = require('dotenv');
var requestJSON = require('request-json');
var xlsx = require('xlsx');

dotenv.load({ path: '.env' });

const api_key = process.env.API_KEY;
const api_context = process.env.API_CONTEXT;

function createBrand(req, res) {
    var httpClient = requestJSON.createClient(api_context);
    httpClient.get('MARCAS?' + api_key,
    function (err, respuestaMLab, body) {
        idMarca = body.length + 1;
        var marca = {
            'codigo': req.body.codigo,
            'valor': req.body.valor
        };
        httpClient.post('MARCAS?' + api_key, marca,
            function (err, respuestaMLab, body) {
                console.log(body);
                res.status(201).send(body);
            });
    });

};

function getBrand(req, res) {
    try {
        let idMarca= req.params.id;
        console.log('idMarca',idMarca);
        let marca = new Object();
        marca.codigo = idMarca == undefined ? undefined : idMarca;
        console.log(marca);
        let query = 'q=' + JSON.stringify(marca) + '&';
        console.log('query'+query);
        let httpClient = requestJSON.createClient(api_context);
        httpClient.get('MARCAS?' + query + api_key,
            function (err, respuestaMLab, body) {
                console.log(body)
                if (body[0] == undefined) return res.status(200).send({ mensaje: "No se encontró resultados" })
                var parametriaArray = new Array();
                var cont = 1;
                body.forEach(function (value) {
                    var parametria = new Object();
                    parametria.nrofila = cont++;
                    parametria.codigo = value.codigo;
                    parametria.valor = value.valor;
                    parametriaArray.push(parametria)
                });
                res.status(200).send(parametriaArray);
            });
    } catch (error) {
        res.status(500).json(error);
    }
};

function deleteBrand (req, res) {
    try {
        let idMarca = req.params.id;
       // let marca = new Object();
       // marca._id.$oid = idMarca == undefined ? undefined : idMarca;
        let marca = { codigo: idMarca};
        let query = 'q=' + JSON.stringify(marca) + '&';
        let httpClient = requestJSON.createClient(api_context);
        httpClient.get('MARCAS?' + query + api_key,
            function (err, respuestaMLab, body) {
                if (body[0] == undefined) return res.status(200).send({ mensaje: "No se encontraron resultados" })
                let deleterMarca = [{}];
                let marca2 = { _id: body[0]._id };
                let query2 = 'q=' + JSON.stringify(marca2) + '&';
                console.log(query2 + api_key);
                httpClient.delete('MARCAS/'+ body[0]._id.$oid +'?' + api_key, deleterMarca,
                    function (err, respuestaMLab, body) {
                        if (err)  res.status(200).json(error);
                        res.status(200).json({mensaje: "Marca eliminada correctamente"});
                    });
            });
    } catch (error) {
        res.status(500).json(error);
    }
}

function updateBrand(req, res) {
    try {
        let idMarca = req.params.id;
        let marca = new Object();

        marca.codigo = idMarca == undefined ? undefined : idMarca;
        var query = 'q=' + JSON.stringify(marca) + '&';
        let httpClient = requestJSON.createClient(api_context);
        console.log('query: ', query)
        httpClient.get('MARCAS?' + query + api_key,
            function (err, respuestaMLab, body) {
                console.log(body)
                if (body[0] == undefined) return res.status(200).send({ mensaje: "No se encontró resultados" })
                let brand = body[0];
                brand.valor = req.body.valor == "" ? "" : req.body.valor,
                brand.codigo = req.body.codigo == "" ? "" : req.body.codigo,
                console.log(brand);
                httpClient.put('MARCAS?' + query + api_key, brand,
                    function (err, respuestaMLab, body) {
                        if (err) return res.status(500).send({ mensaje: "Error actualizando usuario" })
                        res.status(200).send({ mensaje: "Actualizado correctamente" });
                    });
            });
    } catch (error) {
        res.status(500).json(error);
    }
};

exports.getBrand = getBrand;
exports.createBrand = createBrand;
exports.deleteBrand = deleteBrand;
exports.updateBrand = updateBrand;
