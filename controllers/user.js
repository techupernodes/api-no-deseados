const dotenv = require('dotenv');
var requestJSON = require('request-json');
const bcrypt = require('bcryptjs');
dotenv.load({ path: '.env' });

const api_key = process.env.API_KEY;
const api_context = process.env.API_CONTEXT;
var BCRYPT_SALT_ROUNDS = 12;

function createUser(req, res) {
    var httpClient = requestJSON.createClient(api_context);
    var passwordhash;
    httpClient.get('users?' + api_key,
        function (err, respuestaMLab, body) {
            idUser = body.length + 1;
            bcrypt.hash(req.body.password, BCRYPT_SALT_ROUNDS)
                .then(function (hashedPassword) {
                    passwordhash = hashedPassword;
                    var user = {
                        'ID': idUser,
                        'first_name': req.body.firstname,
                        'last_name': req.body.lastname,
                        'email': req.body.email,
                        'password': passwordhash
                    };
                    httpClient.post('users?' + api_key, user,
                        function (err, respuestaMLab, body) {
                            console.log(body);
                            res.status(201).send(body);
                        });
                }).catch(function (error) {
                    console.log("Error creando user: ");
                    console.log(error);
                    next();
                });

        });

};

exports.createUser = createUser;

