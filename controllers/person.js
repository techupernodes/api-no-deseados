const dotenv = require('dotenv');
const requestJSON = require('request-json');
const xlsx = require('xlsx');
const path = require('path');
var pathRoot = require('../app');

dotenv.load({ path: '.env' });

const api_key = process.env.API_KEY;
const api_context = process.env.API_CONTEXT;

function createPerson(req, res) {
    var httpClient = requestJSON.createClient(api_context);
    httpClient.get('PERSONAS?' + api_key,
        function (err, respuestaMLab, body) {
            let person = {
                'codcentral': req.body.codcentral == undefined ? "" : req.body.codcentral,
                'tipdoc': req.body.tipdoc == undefined ? "" : req.body.tipdoc,
                'numdoc': req.body.numdoc == undefined ? "" : req.body.numdoc,
                'fecnacimiento': req.body.fecnacimiento == undefined ? "" : req.body.fecnacimiento,
                'apepaterno': req.body.apepaterno == undefined ? "" : req.body.apepaterno,
                'apematerno': req.body.apematerno == undefined ? "" : req.body.apematerno,
                'nombres': req.body.nombres == undefined ? "" : req.body.nombres,
                'gestoralta': req.body.gestoralta == undefined ? "" : req.body.gestoralta,
                'paisresidencia': req.body.paisresidencia == undefined ? "" : req.body.paisresidencia,
                'paisdomicilio': req.body.paisdomicilio == undefined ? "" : req.body.paisdomicilio,
                'sexo': req.body.sexo == undefined ? "" : req.body.sexo,
                'fecaltacli': req.body.fecaltacli == undefined ? "" : req.body.fecaltacli,
                'ofialtacli': req.body.ofialtacli == undefined ? "" : req.body.ofialtacli,
                'useraltacli': req.body.useraltacli == undefined ? "" : req.body.useraltacli,
                'marcas': req.body.marcas == undefined ? "" : req.body.marcas,
                'origen': req.body.origen == undefined ? "" : req.body.origen,
                'fechmodif': new Date()
            };
            httpClient.post('PERSONAS?' + api_key, person,
                function (err, respuestaMLab, body) {
                    res.status(201).json({ id: body._id.$oid });
                });
        });

}

function searchPerson(req, res) {
    try {
        console.log(req.body.apepaterno)
        let person = new Object();
        person.codcentral = req.body.codcentral == "" ? undefined : req.body.codcentral;
        person.nombres = req.body.nombres == "" ? undefined : req.body.nombres;
        person.apepaterno = req.body.apepaterno == "" ? undefined : req.body.apepaterno;
        person.apematerno = req.body.apematerno == "" ? undefined : req.body.apematerno;
        person.tipdoc = req.body.tipdoc == "" ? undefined : req.body.tipdoc;
        person.numdoc = req.body.numdoc == "" ? undefined : req.body.numdoc;
        person.paisdomicilio = req.body.paisdomicilio == "" ? undefined : req.body.paisdomicilio;

        person.paisresidencia = req.body.paisresidencia == "" ? undefined : req.body.paisresidencia;
        person.marcas = req.body.marcas == "" ? undefined : req.body.marcas;

        var query;
        let httpClient = requestJSON.createClient(api_context);
        console.log("personas nombres: " + person.apepaterno);
        httpClient.headers['Content-Type'] = "application/json;charset=UTF-8";

        //Se aplica búsqueda aproximada por nombres y apellido paterno en caso se ingrese.
        if(person.nombres && person.apepaterno){
            query = 'q={"nombres" : {"$regex" : "' + person.nombres + '*"},'+
                      '"apepaterno" : {"$regex" : "' + person.apepaterno + '*"}}&';
            console.log(query)
        } else {
            query = 'q=' + JSON.stringify(person) + '&';
        }
        console.log(query);
        httpClient.get('PERSONAS?' + query + api_key,
                function (err, respuestaMLab, body) {
                    if (body[0] == undefined) return res.status(200).send({ mensaje: "No se encontró resultados" });
                    res.status(200).json(getPersonArray(body,person));
                });
    } catch (error) {
        res.status(500).json(error);
    }
}

function getPersonArray(body, filtro){
    var personArray = new Array();
    let cont = 1;
    
    body.forEach(function (value) {
        //En caso hayan resultados por aproximación de nombres y apellidos
        //se aplican el resto de filtros.
        if( ( (value.codcentral == filtro.codcentral) || !filtro.codcentral) &&
            ( (value.apematerno == filtro.apematerno) || !filtro.apematerno) &&
            ( (value.tipdoc == filtro.tipdoc) || !filtro.tipdoc) &&
            ( (value.numdoc == filtro.numdoc) || !filtro.numdoc) &&
            ( (value.paisresidencia == filtro.paisresidencia) || !filtro.paisresidencia) &&
            ( (value.paisdomicilio == filtro.paisdomicilio) || !filtro.paisdomicilio) &&
            ( (filtro.marcas && JSON.stringify(value.marcas.sort())==JSON.stringify(filtro.marcas.sort())) || !filtro.marcas)){
            
            let person = new Object();
            person.id = value._id.$oid;
            person.codcentral = value.codcentral;
            person.tipdoc = value.tipdoc;
            person.numdoc = value.numdoc;
            person.fecnacimiento = value.fecnacimiento;
            person.apepaterno = value.apepaterno;
            person.apematerno = value.apematerno;
            person.nombres = value.nombres;
            person.gestoralta = value.gestoralta;
            person.paisresidencia = value.paisresidencia;
            person.paisdomicilio = value.paisdomicilio;
            person.sexo = value.sexo;
            person.fecaltacli = value.fecaltacli;
            person.ofialtacli = value.ofialtacli;
            person.marcas = value.marcas;
            person.origen = value.origen;
            person.fechmodif = value.fechmodif;
            person.nrofila = cont++;
            person.aproximacion = (filtro.nombres != undefined && filtro.apepaterno != undefined) && 
                !(value.nombres == filtro.nombres && value.apepaterno == filtro.apepaterno);
            personArray.push(person);
        }
        
    });
    return personArray;
}

function getPerson(req, res) {
    try {
        let idPerson = req.params.id;
        let person = { _id: { $oid: idPerson } };
        person = idPerson == undefined ? {} : person;
        let query = 'q=' + JSON.stringify(person) + '&';
        let httpClient = requestJSON.createClient(api_context);
        //httpClient.get('PERSONAS?' + query + 'sk=0&l=10&'  + api_key, paginado
        httpClient.get('PERSONAS?' + query + api_key,
            function (err, respuestaMLab, body) {
                console.log(body)
                if (body[0] == undefined) return res.status(200).send({ mensaje: "No se encontró resultados" })
                var personArray = new Array();
                body.forEach(function (value) {
                    var person = new Object();
                    person.id = value._id.$oid;
                    person.codcentral = value.codcentral;
                    person.tipdoc = value.tipdoc;
                    person.numdoc = value.numdoc;
                    person.fecnacimiento = value.fecnacimiento;
                    person.apepaterno = value.apepaterno;
                    person.apematerno = value.apematerno;
                    person.nombres = value.nombres;
                    person.gestoralta = value.gestoralta;
                    person.paisresidencia = value.paisresidencia;
                    person.paisdomicilio = value.paisdomicilio;
                    person.sexo = value.sexo;
                    person.fecaltacli = value.fecaltacli;
                    person.ofialtacli = value.ofialtacli;
                    person.marcas = value.marcas;
                    person.origen = value.origen;
                    person.fechmodif = value.fechmodif;
                    personArray.push(person)
                });
                res.status(200).json(personArray);
            });
    } catch (error) {
        res.status(500).json(error);
    }
}

function updatePerson(req, res) {
    try {
        let idPerson = req.params.id;
        let person = { _id: { $oid: idPerson } };
        person = idPerson == undefined ? {} : person;
        var query = 'q=' + JSON.stringify(person) + '&';
        let httpClient = requestJSON.createClient(api_context);
        //httpClient.get('PERSONAS?' + query + 'sk=0&l=10&'  + api_key, paginado
        httpClient.get('PERSONAS?' + query + api_key,
            function (err, respuestaMLab, body) {
                console.log(body)
                if (body[0] == undefined) return res.status(200).send({ mensaje: "No se encontró resultados" })
                let person = {
                    'codcentral': req.body.codcentral == "" ? "" : req.body.codcentral,
                    'tipdoc': req.body.tipdoc == "" ? "" : req.body.tipdoc,
                    'numdoc': req.body.numdoc == "" ? "" : req.body.numdoc,
                    'fecnacimiento': req.body.fecnacimiento == "" ? "" : req.body.fecnacimiento,
                    'apepaterno': req.body.apepaterno == "" ? "" : req.body.apepaterno,
                    'apematerno': req.body.apematerno == "" ? "" : req.body.apematerno,
                    'nombres': req.body.nombres == "" ? "" : req.body.nombres,
                    'gestoralta': req.body.gestoralta == "" ? "" : req.body.gestoralta,
                    'paisresidencia': req.body.paisresidencia == "" ? "" : req.body.paisresidencia,
                    'paisdomicilio': req.body.paisdomicilio == "" ? "" : req.body.paisdomicilio,
                    'sexo': req.body.sexo == "" ? "" : req.body.sexo,
                    'fecaltacli': req.body.fecaltacli == "" ? "" : req.body.fecaltacli,
                    'ofialtacli': req.body.ofialtacli == "" ? "" : req.body.ofialtacli,
                    'useraltacli': req.body.useraltacli == "" ? "" : req.body.useraltacli,
                    'origen': req.body.origen == "" ? "" : req.body.origen,
                    'marcas': req.body.marcas == "" ? "" : req.body.marcas,
                    'fechmodif': new Date()
                };

                httpClient.put('PERSONAS?' + query + api_key, person,
                    function (err, respuestaMLab, body) {
                        if (err) return res.status(500).send({ mensaje: "Error actualizando usuario" })
                        res.status(200).send({ mensaje: "Actualizado correctamente" });
                    });
            });
    } catch (error) {
        res.status(500).json(error);
    }
}

function deletePersona(req, res) {
    try {
        let idPerson = req.params.idPerson;
        let person = { _id: { $oid: idPerson } };
      //  var query = 'q={"codigo":' + id + '}&';
        let query = 'q=' + JSON.stringify(person) + '&';
        let httpClient = requestJSON.createClient(api_context);
        httpClient.get('PERSONAS?' + query + api_key,
            function (err, respuestaMLab, body) {
                console.log(body[0])
                if (body[0] == undefined) return res.status(200).send({ mensaje: "No se encontraron resultados" })
                let deleterPerson = [{}];
                httpClient.delete('PERSONAS/' + idPerson + '?' + api_key, deleterPerson,
                    function (err, respuestaMLab, body) {
                        if (err) return res.status(200).json(error);
                        res.status(200).json({ mensaje: "Persona eliminada correctamente" });

                    });
            });
    } catch (error) {
        res.status(500).json(error);
    }
}

function readExcelPerson(req, res) {

    try {
        var file = req.file.filename;
        var data = JSON.parse(req.body.json);
        var marcas = data.marcas;
        var origen = data.origen;
        let httpClient = requestJSON.createClient(api_context);
        httpClient.get('PERSONAS?' + api_key,
            function (err, respuestaMLab, body) {
                let excel = xlsx.readFile(path.join(pathRoot.path, '', file), { type: 'base64' });
                let excelRead = excel.SheetNames;
                let excelObject = xlsx.utils.sheet_to_json(excel.Sheets[excelRead[0]]);
                var personArray = new Array();
                excelObject.forEach(function (obj) {
                    var person = new Object();
                    person.codcentral = obj.codcentral == "" ? "" : obj.codcentral;
                    person.tipdoc = obj.tipdoc == "" ? "" : obj.tipdoc;
                    person.numdoc = obj.numdoc == "" ? "" : obj.numdoc;
                    person.fecnacimiento = obj.fecnacimiento == "" ? "" : obj.fecnacimiento;
                    person.apepaterno = obj.apepaterno == "" ? "" : obj.apepaterno;
                    person.apematerno = obj.apematerno == "" ? "" : obj.apematerno;
                    person.nombres = obj.nombres == "" ? "" : obj.nombres;
                    person.gestoralta = obj.gestoralta == "" ? "" : obj.gestoralta;
                    person.paisresidencia = obj.paisresidencia == "" ? "" : obj.paisresidencia;
                    person.paisdomicilio = obj.paisdomicilio == "" ? "" : obj.paisdomicilio;
                    person.sexo = obj.sexo == "" ? "" : obj.sexo;
                    person.fecaltacli = obj.fecaltacli == "" ? "" : obj.fecaltacli;
                    person.ofialtacli = obj.ofialtacli == "" ? "" : obj.ofialtacli;
                    person.useraltacli = obj.useraltacli == "" ? "" : obj.useraltacli;
                    person.marcas =marcas;
                    person.origen = origen;
                    person.fechmodif = new Date();
                    personArray.push(person)
                    console.log("personas " + JSON.stringify(person))
                });

                console.log("array de personas " + JSON.stringify(personArray))
                var jsonArray = JSON.parse(JSON.stringify(personArray))

                httpClient.post('PERSONAS?' + api_key, jsonArray,
                    function (err, respuestaMLab, body) {
                        console.log(body);
                        if (err) return res.status(500).json(err)
                        let carga = {
                            'nombrefile': 'prueba',
                            'fechcreacion': new Date(),

                        };
                        httpClient.post('CARGA?' + api_key, carga,
                            function (err, respuestaMLab, body) {
                                console.log(body);
                                if (err) return res.status(500).json(err)
                                res.json({ mensaje: "Se registró correctamente" });
                            });
                    });
            });

    } catch (error) {
        res.status(500).json(error);
    }

}


exports.getPerson = getPerson;
exports.readExcel = readExcelPerson;
exports.searchPerson = searchPerson;
exports.createPerson = createPerson;
exports.deletePersona = deletePersona;
exports.updatePerson = updatePerson;