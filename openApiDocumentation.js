module.exports = {
  openapi: '3.0.1',
  info: {
    version: '1.3.0',
    title: 'API PERSONAS NO DESEADAS',
    description: 'NIVEL PRACTITIONER',
  },
  servers: [
    {
      url: 'http://localhost:8000/api/v2',
      description: 'Servidor local'
    },
    {
      url: 'https://techu-practitioner.herokuapp.com/api/v2',
      description: 'Servidor de produccion'
    }
  ],
  security: [
    {
      ApiKeyAuth: []
    }
  ],
  tags: [
    {
      name: 'Auth operaciones'
    },
    {
      name: 'Personas operaciones'
    },
    {
      name: 'Parametria operaciones'
    }
  ],
  paths: {

    '/login': {
      post: {
        tags: ['Auth operaciones'],
        description: 'Autentificando api',
        operationId: 'loginPerson',
        requestBody: {
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/Users'
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Autentificando api personas no deseadas',
          }
        }
      }
    },
    '/personas': {
      get: {
        tags: ['Personas operaciones'],
        description: 'Obtener todas las personas no deseadas',
        operationId: 'getPersons',
        responses: {
          '200': {
            description: 'Obteniendo personas no deseadas',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Persons'
                }
              }
            }
          }
        }
      },
      post: {
        tags: ['Personas operaciones'],
        description: 'Create person',
        operationId: 'createPerson',
        requestBody: {
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/PersonCreate'
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Creando Nueva Persona no deseada'
          }
        }
      }
    },
    '/personas/{idperson}': {
      get: {
        tags: ['Personas operaciones'],
        description: 'Buscar por id de la persona',
        operationId: 'findByIdPerson',
        parameters: [
          {
            name: 'idperson',
            in: 'path',
            schema: {
              type: 'String',
              default: '5de58023443dbd0c7c4ace80'
            },
            required: false
          }
        ],
        responses: {
          '200': {
            description: 'Obteniendo persona no deseada por id',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Person'
                }
              }
            }
          }
        }
      },
      put: {
        tags: ['Personas operaciones'],
        description: 'Buscar por id de la persona',
        operationId: 'updateByIdPerson',
        parameters: [
          {
            name: 'idperson',
            in: 'path',
            schema: {
              type: 'String',
              default: '5de58023443dbd0c7c4ace80'
            },
            required: false
          }
        ],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/PersonCreate'
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Actualizando persona no deseada por id',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Mensaje'
                }
              }
            }
          }
        }
      },
      delete: {
        tags: ['Personas operaciones'],
        description: 'Eliminar por id de la persona',
        operationId: 'deleteByIdPerson',
        parameters: [
          {
            name: 'idperson',
            in: 'path',
            schema: {
              type: 'String',
              default: '5de58023443dbd0c7c4ace80'
            },
            required: false
          }
        ],
        responses: {
          '200': {
            description: 'Eliminar persona no deseada por id',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/MensajeDelete'
                }
              }
            }
          }
        }
      }
    },
    '/personas/search': {
      post: {
        tags: ['Personas operaciones'],
        description: 'busqueda de personas',
        operationId: 'searchPerson',
        requestBody: {
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/PersonSearch'
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Autentificando api personas no deseadas',
          }
        }
      },
    },
    '/parametrias/{codigo}': {
      get: {
        tags: ['Parametria operaciones'],
        description: 'Buscar por id de la persona',
        operationId: 'findByCodigo',
        parameters: [
          {
            name: 'codigo',
            in: 'path',
            schema: {
              type: 'String',
              default: '03'
            },
            required: false
          }
        ],
        responses: {
          '200': {
            description: 'Obteniendo persona no deseada por id',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Parametria'
                }
              }
            }
          }
        }
      },
      delete: {
        tags: ['Parametria operaciones'],
        description: 'Eliminar por id de la persona',
        operationId: 'deleteByCodigo',
        parameters: [
          {
            name: 'codigo',
            in: 'path',
            schema: {
              type: 'String',
              default: '03'
            },
            required: false
          }
        ],
        responses: {
          '200': {
            description: 'Eliminar persona no deseada por id',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/MensajeDelete'
                }
              }
            }
          }
        }
      },
      put: {
        tags: ['Parametria operaciones'],
        description: 'Buscar por id de la persona',
        operationId: 'updateByCodigo',
        parameters: [
          {
            name: 'codigo',
            in: 'path',
            schema: {
              type: 'String',
              default: '03'
            },
            required: false
          }
        ],
        requestBody: {
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/ParametriaCreate'
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Actualizando persona no deseada por id',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Mensaje'
                }
              }
            }
          }
        }
      }
    },
    '/parametrias': {
      get: {
        tags: ['Parametria operaciones'],
        description: 'Obtener todas las personas no deseadas',
        operationId: 'getParametria',
        responses: {
          '200': {
            description: 'Obteniendo personas no deseadas',
            content: {
              'application/json': {
                schema: {
                  $ref: '#/components/schemas/Parametrias'
                }
              }
            }
          }
        }
      },
      post: {
        tags: ['Parametria operaciones'],
        description: 'Create person',
        operationId: 'createParametria',
        requestBody: {
          content: {
            'application/json': {
              schema: {
                $ref: '#/components/schemas/ParametriaCreate'
              }
            }
          },
          required: true
        },
        responses: {
          '200': {
            description: 'Creando Nueva Persona no deseada'
          }
        }
      }
    },
  },
  components: {
    schemas: {
      nrofila: {
        type: 'integer',
        example: 1
      },
      codigo: {
        type: 'string',
        example: '03'
      },
      valor: {
        type: 'string',
        example: 'POLICIAL'
      },
      mensaje: {
        type: 'string',
        example: 'Actualizado correctamente'
      },
      mensajeeliminado: {
        type: 'string',
        example: 'Elminado correctamente'
      },
      email: {
        type: 'string',
        description: 'email',
        example: 'maribelmcs@gmail.com'
      },
      password: {
        type: 'string',
        description: 'password',
        example: 'mipass5'
      },
      id: {
        type: 'string',
        description: 'id autogenerado mongo db',
      },
      codcentral: {
        type: 'string',
        description: 'codigo en caso de ser cliente del BBVA',
        example: '45276723'
      },
      tipodoc: {
        type: 'string',
        description: 'Si es persona natural o juridica',
        example: 'L'
      },
      numdoc: {
        type: 'string',
        description: 'Numero de documento',
        example: '45276723'
      },
      fecnacimiento: {
        type: 'date',
        description: 'fecha de nacimiento',
        example: '28/10/1995'
      },
      apepaterno: {
        type: 'string',
        description: 'apellido paterno',
        example: 'RIVERA'
      },
      apematerno: {
        type: 'string',
        description: 'apellido materno',
        example: 'LEON'
      },
      nombres: {
        type: 'string',
        description: 'nombres completos',
        example: 'PAUL GIANMARCO'
      },
      gestoralta: {
        type: 'string',
        description: 'gestor que le dio de alta',
        example: 'FELIPE ROJAS'
      },
      paisresidencia: {
        type: 'string',
        description: 'pais de residencia',
        example: 'PERU'
      },
      paisdomicilio: {
        type: 'string',
        description: 'pais de domicilio',
        example: 'PERU'
      },
      sexo: {
        type: 'string',
        description: 'masculino o femenino',
        example: 'M'
      },
      fecaltacli: {
        type: 'string',
        description: 'fecha alta del cliente',
        example: '26/02/2017'
      },
      ofialtacli: {
        type: 'string',
        description: 'oficina alta del cliente',
        example: 'ATE'
      },
      useraltacli: {
        type: 'string',
        description: 'usuario alta cliente',
        example: 'P026094'
      },
      origen: {
        type: 'string',
        description: 'desde donde se ingreso la persona no deseada',
        example: 'COMEX AND LEASING'
      },
      marcas: {
        type: 'string',
        description: 'marcas asignadas',
      },
      fechmodif: {
        type: 'date',
        description: 'fecha de modificacion',
        example: '28/11/2019'
      },
      PersonCreate: {
        type: 'object',
        properties: {
          codcentral: {
            $ref: '#/components/schemas/codcentral'
          },
          tipodoc: {
            $ref: '#/components/schemas/tipodoc'
          },
          numdoc: {
            $ref: '#/components/schemas/numdoc'
          },
          fecnacimiento: {
            $ref: '#/components/schemas/fecnacimiento'
          },
          apepaterno: {
            $ref: '#/components/schemas/apepaterno'
          },
          apematerno: {
            $ref: '#/components/schemas/apematerno'
          },
          nombres: {
            $ref: '#/components/schemas/nombres'
          },
          gestoralta: {
            $ref: '#/components/schemas/gestoralta'
          },
          paisresidencia: {
            $ref: '#/components/schemas/paisresidencia'
          },
          paisdomicilio: {
            $ref: '#/components/schemas/paisdomicilio'
          },
          sexo: {
            $ref: '#/components/schemas/sexo'
          },
          fecaltacli: {
            $ref: '#/components/schemas/fecaltacli'
          },
          ofialtacli: {
            $ref: '#/components/schemas/ofialtacli'
          },
          useraltacli: {
            $ref: '#/components/schemas/useraltacli'
          },
          origen: {
            $ref: '#/components/schemas/origen'
          },
          marcas: {
            $ref: '#/components/schemas/marcas'
          },
          fechmodif: {
            $ref: '#/components/schemas/fechmodif'
          }
        }
      },
      PersonSearch: {
        type: 'object',
        properties: {
          codcentral: {
            $ref: '#/components/schemas/codcentral'
          },
          tipodoc: {
            $ref: '#/components/schemas/tipodoc'
          },
          numdoc: {
            $ref: '#/components/schemas/numdoc'
          },
          fecnacimiento: {
            $ref: '#/components/schemas/fecnacimiento'
          },
          apepaterno: {
            $ref: '#/components/schemas/apepaterno'
          },
          apematerno: {
            $ref: '#/components/schemas/apematerno'
          },
          nombres: {
            $ref: '#/components/schemas/nombres'
          },
          paisdomicilio: {
            $ref: '#/components/schemas/paisdomicilio'
          }
        }
      },
      Person: {
        type: 'object',
        properties: {

          id: {
            $ref: '#/components/schemas/id'
          },
          codcentral: {
            $ref: '#/components/schemas/codcentral'
          },
          tipodoc: {
            $ref: '#/components/schemas/tipodoc'
          },
          numdoc: {
            $ref: '#/components/schemas/numdoc'
          },
          fecnacimiento: {
            $ref: '#/components/schemas/fecnacimiento'
          },
          apepaterno: {
            $ref: '#/components/schemas/apepaterno'
          },
          apematerno: {
            $ref: '#/components/schemas/apematerno'
          },
          nombres: {
            $ref: '#/components/schemas/nombres'
          },
          gestoralta: {
            $ref: '#/components/schemas/gestoralta'
          },
          paisresidencia: {
            $ref: '#/components/schemas/paisresidencia'
          },
          paisdomicilio: {
            $ref: '#/components/schemas/paisdomicilio'
          },
          sexo: {
            $ref: '#/components/schemas/sexo'
          },
          fecaltacli: {
            $ref: '#/components/schemas/fecaltacli'
          },
          ofialtacli: {
            $ref: '#/components/schemas/ofialtacli'
          },
          useraltacli: {
            $ref: '#/components/schemas/useraltacli'
          },
          origen: {
            $ref: '#/components/schemas/origen'
          },
          marcas: {
            $ref: '#/components/schemas/marcas'
          },
          fechmodif: {
            $ref: '#/components/schemas/fechmodif'
          }
        }
      },
      Persons: {
        type: 'array',
        items: {
          $ref: '#/components/schemas/Person'
        }
      },
      Mensaje: {
        type: 'object',
        properties: {
          mensaje: {
            $ref: '#/components/schemas/mensaje'
          }
        }
      },
      MensajeDelete: {
        type: 'object',
        properties: {
          mensaje: {
            $ref: '#/components/schemas/mensajeeliminado'
          }
        }
      },
      Parametria: {
        type: 'object',
        properties: {
          codigo: {
            $ref: '#/components/schemas/codigo'
          },
          valor: {
            $ref: '#/components/schemas/valor'
          },
          nrofila: {
            $ref: '#/components/schemas/nrofila'
          }
        }
      },
      ParametriaCreate: {
        type: 'object',
        properties: {
          codigo: {
            $ref: '#/components/schemas/codigo'
          },
          valor: {
            $ref: '#/components/schemas/valor'
          }
        }
      },
      Parametrias: {
        type: 'array',
        items: {
          $ref: '#/components/schemas/Parametria'
        }
      },
      Users: {
        type: 'object',
        properties: {
          email: {
            $ref: '#/components/schemas/email'
          },
          password: {
            $ref: '#/components/schemas/password'
          }
        }
      },
      Error: {
        type: 'object',
        properties: {
          message: {
            type: 'string'
          },
          internal_code: {
            type: 'string'
          }
        }
      }
    },

    securitySchemes: {
      ApiKeyAuth: {
        type: 'apiKey',
        in: 'header',
        name: 'tokentechu'
      }
    }
  }
};