# API RESTful No Deseados

Este API RESTful está desplegado en un servicio en cloud de Heroku : [API REST Heroku] (https://techu-practitioner.herokuapp.com/api/v2/)

Todo el codigo esta alojado en GitHub en el siguiente repositorio: [API REST ] (https://bitbucket.org/techupernodes/api-no-deseados/)

## Introducción
Este API esta desarrollado para para permitir la carga y consulta de personas no deseadas, asi como las parametrías relacionadas.


La documentación del API se encuentra disponible en:
- Documentacion del API REST a partir de la herramienta SWAGGER, para ver la documentacion hay que ir al siguiente enlace: (https://techu-practitioner.herokuapp.com/api-docs)

#### Para poner en marcha la aplicación tendremos que realizar los siguientes comandos:

 `npm install 		 //con esto instalaremos los modulos necesarios para node.js`


Una vez ejecutado el servidor de MongoDB, comenzaremos con la ejecucion de la aplicacion por medio de uno de estos dos comandos:

Si solo vamos a probar la aplicacion es recomendable este:

`npm start`

##### Para realizar las pruebas usaremos una aplicacion llamada PostMan, esta aplicacion permite realizar las peticiones necesarias para un API REST (GET, POST, DELETE, PUT).
