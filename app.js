const express = require('express');
const cors = require('cors');
const logger = require('morgan');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
const app = express();
const swaggerUi = require('swagger-ui-express');
const openApiDocumentation = require('./openApiDocumentation');

app.use(express.static(__dirname + '/public/upload/'));


var config = require('./config/config');
var URL_BASE = config.variables.URL_BASE;
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(openApiDocumentation));

var path = `${__dirname}/public/upload/`;

dotenv.load({ path: '.env' });

app.use(logger('dev'));



app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(bodyParser.json({limit: '50mb', extended: true}));
app.use(cors());

//routers folders
const homeRoutes = require('./routes/auth');
const personRoutes = require('./routes/person');
const brandRoutes = require('./routes/parametria');
const cargaRoutes = require('./routes/carga');
const userRoutes = require('./routes/user');

app.use(URL_BASE, homeRoutes);
app.use(URL_BASE + 'personas', personRoutes);
app.use(URL_BASE + 'parametrias', brandRoutes);
app.use(URL_BASE + 'cargas', cargaRoutes);
app.use(URL_BASE + 'usuarios', userRoutes);


exports.path = path;

module.exports = app;