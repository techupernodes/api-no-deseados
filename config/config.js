const variables_globales = new Object();

variables_globales.URL_CONTEXT = '/api/';
variables_globales.API_VERSION = 'v2/';
variables_globales.URL_BASE = variables_globales.URL_CONTEXT + variables_globales.API_VERSION;



module.exports.variables = variables_globales;