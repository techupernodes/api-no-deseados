const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  try {
    console.log(req.headers.tokentechu)
    const token = req.headers.tokentechu;
    const decoded = jwt.verify(token, process.env.API_KEY_TECHU);
    req.data = decoded;
    next();
  } 
  catch (error) {
    console.log(error)
    return res.status(401).json({
        message: 'Auth failed'
    });
  }
};