const multer = require('multer');
module.exports = (req, res, next) => {
    try {
    

        const storage = multer.diskStorage({
            destination : function(req,file,callback){
                callback(null, '../public/upload/');
            },
            filename: function(req,file,callback){
                callback(null, file.fieldname + '-' + Date.now());
            }
        });

        const upload = multer({ storage : storage});
       

      next();
    } 
    catch (error) {
      console.log(error)
      return res.status(401).json({
          message: 'Error con cargar archivo'
      });
    }
  };


